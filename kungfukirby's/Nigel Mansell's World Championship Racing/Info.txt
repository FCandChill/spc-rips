Here's an untagged one for Nigel Mansell's World Championship Racing, that being the racing theme. The track is SNES-clocked, so I replicated it. However, I ended up using the cheap way out of using a single timer value, rather than try to go for more exact NTSC specifications.

This one wasn't released on SNESMusic.org.
